// What is MongoDB Atlas?
	// MongoDB Atlas is a MongoDB database in the cloud. It is the cloud service of MongoDB, the leading NoSQL database.

// What is Robo 3T?
	// Robo3T is an application which allows the use of GUI to manipulate MongoDB databases. The advantage of Robo3T in just using Atlas is that Robo3T is a local application.

// What is Shell?
	// Shell is an interface in MongoDB that allows us to input commands and perform CRUD operations in our database.
	// (right click on connention/Batch 169 and open shell.)

// CRUD Operations
	// Create, Read, Update, and Delete

	// Create - inserting documents

		// insertOne({})

			/*
				Syntax:
				db.collection.insertOne({
					"fieldA" : "value A",
					"fieldB" : "value B",
				}); - allows us to insert a document into a collection
			*/

			db.users.insertOne({
				"firstName" : "Jean",
				"lastName" : "Smith",
				"age" : 24,
				"contact" : {
					"phone" : "09123456789",
					"email" : "js@mail.com"
				},
				"courses" : [
					"CSS", "JavaScript", "ReactJS"
				],
				"department" : "none"
			});

		// insertMany({})
			/*
				Syntax:
				db.collections.insertMany([
					{objectA}, {objectB}
				])
			*/

			db.users.insertMany([
				{
					"firstName" : "Stephen",
					"lastName" : "Hawking",
					"age" : 76,
					"contact" : {
						"phone" : "09123456789",
						"email" : "stephenhawking@mail.com"
					},
					"courses" : [
						"Python", "PHP", "React"
					],
					"department" : "none"

				},

				{
					"firstName" : "Neil",
					"lastName" : "Armstrong",
					"age" : 82,
					"contact" : {
						"phone" : "09987654321",
						"email" : "armstrong@mail.com"
					},
					"courses" : [
						"Laravel", "Sass", "Springboot"
					],
					"department" : "none"

				}
			]);
	// Read - find documents
		/*
			Syntax:
			db.collections.find()
				>> will return ALL documents
			db.collections.findOne({})
				>> will return the first document
			db.collections.find({"criteria" : "value"})
				>> will return ALL documents that match the criteria in the collection
			db.collections.findOne({"criteria" : "value"})
				>> will return the first document that matches the criteria
			db.collection.findJ({"fieldA" : "valueA", "fieldB" : "valueB"})
		*/

		db.users.find()

		db.users.findOne({})

		db.users.find({"firstName" : "Jean"})

		db.users.findOne({"firstName" : "Kate"})

		db.users.find({"lastName" : "Armstrong", "age" : 82})

	// Update documents
		/*
			Syntax:
				db.collection.updateOne({"criteria" : "value"}, {$set:{"fieldToBeUpdated" : "updatedValue"}})
					>> updates the first item that matches the criteria

				db.collection.updateOne({}, {$set: {"fieldToBeUpdated" : "updatedValue"}})
					>> updates the first item in the collection

				db.collection.updateMany({"criteria" : "value"}, {$set: {"fieldToBeUpdated" : "updatedValue"}})
					>> updates all items that match the criteria

				db.collection.updateMany({}, {$set: {"fieldToBeUpdated" : "updatedValue"}})
					>> updates all items in the collection
		*/

		db.users.updateOne({"firstName" : "John"}, {$set: {"lastName" : "Gates"}})

		db.users.updateOne({}, {$set: {"emergencyContact" : "father"}})

		db.users.updateMany({"department" : "none"}, {$set: {"department" : "Tech"}})

		db.users.updateMany({}, {$rename: {"dept" : "department"}})
				// (This renames fields.)

		db.users.updateMany({}, {$set: {"emergencyContact" : "mother"}})

	// Delete - to delete documents
		/*
			Syntax:

				db.collection.deleteOne({"criteria" : "value"})
					>> delete the first item that matches the criteria

				db.collection.deleteMany({"criteria" : "value"})
					>> delete ALL items that match the criteria

				db.collection.deleteOne({})
					>> delete the first item in the collection

				db.collection.deleteMany({})
					>> delete ALL items in the collection
					
		*/

		db.users.deleteOne({"firstName" : "Neil"})

		db.users.deleteOne({})

		db.users.deleteMany({"lastName" : "Middleton"})

		db.users.deleteMany({})

		db.users.insertOne({
			"name" : "Kim Jin",
			"birthday" : "12/30/1992",
			"age" : 29
		})

		// db.users.aggregate({}, {$unset: "age"})