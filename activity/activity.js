db.users.insertMany([

	{
		"firstName" : "Corey",
		"lastName" : "Taylor",
		"email" : "coreytaylor@mail.com",
		"password" : "taycorpass",
		"isAdmin" : false
	},

	{
		"firstName" : "Shawn",
		"lastName" : "Crahan",
		"email" : "shawncrahan@mail.com",
		"password" : "shawcrahpass",
		"isAdmin" : false
	},

	{
		"firstName" : "Jim",
		"lastName" : "Root",
		"email" : "jimroot@mail.com",
		"password" : "jimrootpass",
		"isAdmin" : false
	},

	{
		"firstName" : "Mick",
		"lastName" : "Thomson",
		"email" : "mickthomson@mail.com",
		"password" : "mickthompass",
		"isAdmin" : false
	},

	{
		"firstName" : "Craig",
		"lastName" : "Jones",
		"email" : "craigjones@mail.com",
		"password" : "craijonpass",
		"isAdmin" : false
	}

]);


db.courses.insertMany([

	{
		"name" : "Vocals 101",
		"price" : 5000,
		"isActive" : false
	},

	{
		"name" : "Guitar 101",
		"price" : 7000,
		"isActive" : false
	},

	{
		"name" : "Percussion 101",
		"price" : 3000,
		"isActive" : false
	}

]);


db.users.find({"isAdmin": false})

db.users.updateOne({}, {$set: {"isAdmin" : true}})
db.courses.updateOne({"name" : "Guitar 101"}, {$set: {"isActive" : true}})

db.courses.deleteMany({"isActive" : false})